#include <stdio.h>
int main(void)
{
	//variable declarations
	int i_RAD;
	//code
	printf("\n\n");
	printf("Printing Digits 10 to 1 : \n\n");
	for (i_RAD = 10; i_RAD >= 1; i_RAD--)
	{
		printf("\t%d\n", i_RAD);
	}
	printf("\n\n");
	return(0);
}