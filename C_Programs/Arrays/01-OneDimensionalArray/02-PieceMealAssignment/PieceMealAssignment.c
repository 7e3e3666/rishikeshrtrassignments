#include <stdio.h>
int main(void)
{
	//varaible declaration
	int iArrayOne_RAD[10];
	int iArrayTwo_RAD[10];

	//code

	//********iArrayOne[]**********
	iArrayOne_RAD[0] = 3;
	iArrayOne_RAD[1] = 6;
	iArrayOne_RAD[2] = 9;
	iArrayOne_RAD[3] = 12;
	iArrayOne_RAD[4] = 15;
	iArrayOne_RAD[5] = 18;
	iArrayOne_RAD[6] = 21;
	iArrayOne_RAD[7] = 24;
	iArrayOne_RAD[8] = 27;
	iArrayOne_RAD[9] = 30;


	printf("\n\n");
	printf("Piece-meal (HArd-coded) Asignment And Display Of Elements to Array 'iArrayOne[]' : \n\n");
	printf("1st Element of Array 'iArrayOne[]' or Element At 0th Index Of Array 'iArrayOne[]'  = %d\n", iArrayOne_RAD[0]);
	printf("2nd Element of Array 'iArrayOne[]' or Element At 1st Index Of Array 'iArrayOne[]'  = %d\n", iArrayOne_RAD[1]);
	printf("3rd Element of Array 'iArrayOne[]' or Element At 2nd Index Of Array 'iArrayOne[]'  = %d\n", iArrayOne_RAD[2]);
	printf("4th Element of Array 'iArrayOne[]' or Element At 3rd Index Of Array 'iArrayOne[]'  = %d\n", iArrayOne_RAD[3]);
	printf("5th Element of Array 'iArrayOne[]' or Element At 4th Index Of Array 'iArrayOne[]'  = %d\n", iArrayOne_RAD[4]);
	printf("6th Element of Array 'iArrayOne[]' or Element At 5th Index Of Array 'iArrayOne[]'  = %d\n", iArrayOne_RAD[5]);
	printf("7th Element of Array 'iArrayOne[]' or Element At 6th Index Of Array 'iArrayOne[]'  = %d\n", iArrayOne_RAD[6]);
	printf("8th Element of Array 'iArrayOne[]' or Element At 7th Index Of Array 'iArrayOne[]'  = %d\n", iArrayOne_RAD[7]);
	printf("9th Element of Array 'iArrayOne[]' or Element At 8th Index Of Array 'iArrayOne[]'  = %d\n", iArrayOne_RAD[8]);
	printf("10th Element of Array 'iArrayOne[]' or Element At 9th Index Of Array 'iArrayOne[]'  = %d\n", iArrayOne_RAD[9]);

	//**************iArrayTwo[]***************

	printf("\n\n");

	printf("Enter 1st Element of Array 'iArrayTwo[]'  :  ");
	scanf("%d", &iArrayTwo_RAD[0]);
	printf("Enter 2nd Element of Array 'iArrayTwo[]'  :  ");
	scanf("%d", &iArrayTwo_RAD[1]);
	printf("Enter 3rd Element of Array 'iArrayTwo[]'  :  ");
	scanf("%d", &iArrayTwo_RAD[3]);
	printf("Enter 4th Element of Array 'iArrayTwo[]'  :  ");
	scanf("%d", &iArrayTwo_RAD[4]);
	printf("Enter 5th Element of Array 'iArrayTwo[]'  :  ");
	scanf("%d", &iArrayTwo_RAD[5]);
	printf("Enter 6th Element of Array 'iArrayTwo[]'  :  ");
	scanf("%d", &iArrayTwo_RAD[6]);
	printf("Enter 7th Element of Array 'iArrayTwo[]'  :  ");
	scanf("%d", &iArrayTwo_RAD[7]);
	printf("Enter 8th Element of Array 'iArrayTwo[]'  :  ");
	scanf("%d", &iArrayTwo_RAD[8]);
	printf("Enter 9th Element of Array 'iArrayTwo[]'  :  ");
	scanf("%d", &iArrayTwo_RAD[9]);

	printf("\n\n");
	printf("Piece-meal (User Input) Assignment And Display of Elements to Arraym'iArrayTwo[]' :\n\n");
	printf("1st Element of Array 'iArrayTwo[]' or Element At 0th Index Of Array 'iArrayTwo[]' = %d\n", iArrayTwo_RAD[0]);
	printf("2nd Element of Array 'iArrayTwo[]' or Element At 1st Index Of Array 'iArrayTwo[]' = %d\n", iArrayTwo_RAD[1]);
	printf("3rd Element of Array 'iArrayTwo[]' or Element At 2nd Index Of Array 'iArrayTwo[]' = %d\n", iArrayTwo_RAD[2]);
	printf("4th Element of Array 'iArrayTwo[]' or Element At 3rd Index Of Array 'iArrayTwo[]' = %d\n", iArrayTwo_RAD[3]);
	printf("5th Element of Array 'iArrayTwo[]' or Element At 4th Index Of Array 'iArrayTwo[]' = %d\n", iArrayTwo_RAD[4]);
	printf("6th Element of Array 'iArrayTwo[]' or Element At 5th Index Of Array 'iArrayTwo[]' = %d\n", iArrayTwo_RAD[5]);
	printf("7th Element of Array 'iArrayTwo[]' or Element At 6th Index Of Array 'iArrayTwo[]' = %d\n", iArrayTwo_RAD[6]);
	printf("8th Element of Array 'iArrayTwo[]' or Element At 7th Index Of Array 'iArrayTwo[]' = %d\n", iArrayTwo_RAD[7]);
	printf("9th Element of Array 'iArrayTwo[]' or Element At 8th Index Of Array 'iArrayTwo[]' = %d\n", iArrayTwo_RAD[8]);
	printf("10th Element of Array 'iArrayTwo[]' or Element At 9th Index Of Array 'iArrayTwo[]' = %d\n", iArrayTwo_RAD[9]);

	return(0);

}






























