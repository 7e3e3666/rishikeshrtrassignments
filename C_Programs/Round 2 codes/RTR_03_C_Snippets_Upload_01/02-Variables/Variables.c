#include <stdio.h>

int main(void)
{

    // variable declarations
    int i_RAD = 5;           // Assign Value to i (integer)
    float f_RAD = 3.9f;      // Assign Value to f (float)
    double d_RAD = 8.041997; // Assign Value to d (double)
    char c_RAD = 'A';        // Assign Value to c as character 'P'
    
	//code
    printf("\n\n");
    
    printf("\ni_RAD = %d", i_RAD);
    printf("\nf_RAD = %f", f_RAD);
    printf("\nd_RAD = %lf", d_RAD);
    printf("\nc_RAD = %c", c_RAD);

    printf("\n\n");

    i_RAD = 43;         // Assign Value to i (integer)
    f_RAD = 6.54f;      // Assign Value to f (float)
    d_RAD = 26.1294;    // Assign Value to d (double)
    c_RAD = 'P';        // Assign Value to c as character 'P'
    
    printf("\ni_RAD = %d", i_RAD);
    printf("\nf_RAD = %f", f_RAD);
    printf("\nd_RAD = %lf", d_RAD);
    printf("\nc_RAD = %c", c_RAD);

    printf("\n\n");

    return(0);
}






