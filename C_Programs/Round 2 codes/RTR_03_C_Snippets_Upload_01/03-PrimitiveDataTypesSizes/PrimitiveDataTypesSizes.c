#include <stdio.h>

int main(void)
{
	//code
    printf("\n\n");
    
    printf("\nSize Of 'int'          = %ld bytes", sizeof(int));                //signed integer
    printf("\nSize Of 'unsigned int' = %ld bytes", sizeof(unsigned int));       //unsigned integer
    printf("\nSize Of 'long'         = %ld bytes", sizeof(long));               //singend long
    printf("\nSize Of 'long long'    = %ld bytes", sizeof(long long));          //long long
    printf("\nSize Of 'float'        = %ld bytes", sizeof(float));              // signed float
    printf("\nSize Of 'double'       = %ld bytes", sizeof(double));             // signed double
    printf("\nSize Of 'long double'  = %ld bytes", sizeof(long double));        // signed long double

    printf("\n\n");

    return(0);
}








